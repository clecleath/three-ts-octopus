import * as THREE from 'three';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader';
import { randInt } from 'three/src/math/MathUtils';

let betaAnim = 0;
let camera, scene, renderer,raycaster, clock, octopus, dataArray, analyser;
const octoBox = new THREE.Box3();
let planeBoxes: THREE.Box3[] = [];
let yMovement = -0.1;
let zMovement = 0;
let rotationOrder = 1;
let lastRotationOrder = 1;
let bones = new Map <string,Array<THREE.Object3D | undefined>>();
var audio = document.getElementById("audio");
let bonesNames = ["first_bone","second_bone","third_bone","fourth_bone","fifth_bone","sixth_bone","seventh_bone","eight_bone"];
init();
play("assets/music/Ayla.mp3");
animate();

function init(){

    const container = document.createElement( 'div' );
    document.body.appendChild( container );
    const fov = 45;
    const ratio = window.innerWidth / window.innerHeight;
    const znear = 0.25;
    const zfar = 200;
    camera = new THREE.PerspectiveCamera( fov,ratio , znear, zfar );
    camera.position.set( - 1.8, 0.6, 2.7 );

    scene = new THREE.Scene();

    raycaster = new THREE.Raycaster();
    clock = new THREE.Clock();

    //settings names 

    new THREE.TextureLoader()
        .setPath( "/assets/textures/" )
        .load( 'digital_painting_underwater_light.jpg', function ( texture ) {

            texture.mapping = THREE.EquirectangularReflectionMapping;

            scene.background = texture;
            scene.environment = texture;
            render();
            const loader = new GLTFLoader().setPath( '/assets/models/' );

            loader.load( 'pieuvre_clement_athimon_bones_8.glb', function ( gltf ) {
                octopus = gltf.scene;
                scene.add( octopus );

                bonesNames.forEach(name => {
                    bones.set(name,[]);
                    let bone = scene.getObjectByName(name);
                    bones.get(name)?.push(bone);
                    bone = bone.children;
                    while(!(bone === undefined || bone.length == 0)){
                        bones.get(name)?.push(bone[0]);
                        bone = bone[0].children;
                    }   
                });
                octoBox.setFromObject(octopus,true);
                animate();
            } );

    
           
        } );
  

    renderer = new THREE.WebGLRenderer( { antialias: true } );
    renderer.setPixelRatio( window.devicePixelRatio );
    renderer.setSize( window.innerWidth, window.innerHeight );
    renderer.toneMapping = THREE.ACESFilmicToneMapping;
    renderer.toneMappingExposure = 1;
    renderer.outputEncoding = THREE.sRGBEncoding;
    container.appendChild( renderer.domElement );
    
    const controls = new OrbitControls( camera, renderer.domElement );
    controls.addEventListener( 'change', render ); // use if there is no animation loop
    controls.minDistance = 2;
    controls.maxDistance = 200;
    controls.target.set( 0, 0, - 0.2 );
    controls.update();

    window.addEventListener( 'resize', onWindowResize );
    //window.addEventListener('click' ,setClickPosition);
}


function onWindowResize() {

    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();

    renderer.setSize( window.innerWidth, window.innerHeight );

    animate();

}

function render() {

    renderer.render( scene, camera );

}

function animate(){

    requestAnimationFrame(animate);
    
    betaAnim += 0.01;
    let changeposition = 0;
    //let order = 30/7;
    analyser.getByteFrequencyData(dataArray);
    var lowerHalfArray = dataArray.slice(0, (dataArray.length / 2) - 1);
    var upperHalfArray = dataArray.slice((dataArray.length / 2) - 1, dataArray.length - 1);
    /*bones.forEach((arm) => {
        let changeposition = 0;
      /*  if(odd){
            let rank = 0;
            arm.forEach(bone =>{
                let value = Math.floor(lowerHalfArray[rank]/5)*0.005 
                changeposition += 2*(Math.PI/10)+value
                //let value = lowerHalfArray[rank]*0.005 
                //bone?.scale.set(value,value,value)
                bone.position.x = 0.1*Math.sin((changeposition)+betaAnim);
                rank++;
            });
            odd = false;
        }else{
            let rank = 0;
            arm.forEach(bone =>{
                changeposition += 2*(Math.PI/35)
                //let value = lowerHalfArray[rank]*0.005 
                //bone?.scale.set(value,value,value)
                //bone.position.x = 0.1*Math.sin((changeposition)+(upperHalfArray[rank]*4));
                rank++;
            });
            odd = true;
        }


        
        //bone.rotation.z = 0.1*Math.sin((changeposition)+betaAnim);
        //bone.rotation.y = 0.1*Math.sin((changeposition)+betaAnim);
    });*/
    if(octopus != undefined){
        if(Math.floor(max(lowerHalfArray)/230) == 1 && lastRotationOrder!=2){
            rotationOrder = -rotationOrder;
            lastRotationOrder = 2
        }else if(Math.floor(max(lowerHalfArray)/230) == 0 && lastRotationOrder!=1){
            rotationOrder = -rotationOrder;
            lastRotationOrder = 1
        }
        octopus.rotateY(rotationOrder*avg(lowerHalfArray)*(Math.PI/180)*0.004);
    }



    
    renderer.render(scene, camera);
}

function avg(arr) {
    var total = arr.reduce(function (sum, b) { return sum + b; });
    return (total / arr.length);
}

function max(arr) {
    return arr.reduce(function (a, b) { return Math.max(a, b); })
}    

function play(e) {
    console.log(e);
    audio.src = e; // URL.createObjectURL(e);
    audio.load();
    audio.play();


    var context = new AudioContext();
    var src = context.createMediaElementSource(audio);
    analyser = context.createAnalyser();
    src.connect(analyser);
    analyser.connect(context.destination);
    analyser.fftSize = 512;
    var bufferLength = analyser.frequencyBinCount;
    dataArray = new Uint8Array(bufferLength);
}
