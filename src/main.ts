import * as THREE from 'three';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader';
import { randInt } from 'three/src/math/MathUtils';

let betaAnim = 0;
let camera, scene, renderer,raycaster, clock, mixer,trampo,octopus;
const octoBox = new THREE.Box3();
let planeBoxes: THREE.Box3[] = [];
let yMovement = -0.1;
let zMovement = 0;
let quaterRotation = Math.PI*.0001;
let frameCounter = 0;
let bones = new Map <string,Array<THREE.Object3D | undefined>>();
let bonesNames = ["first_bone","second_bone","third_bone","fourth_bone","fifth_bone","sixth_bone","seventh_bone","eight_bone"];
init();
animate();

function init(){

    const container = document.createElement( 'div' );
    document.body.appendChild( container );
    const fov = 45;
    const ratio = window.innerWidth / window.innerHeight;
    const znear = 0.25;
    const zfar = 200;
    camera = new THREE.PerspectiveCamera( fov,ratio , znear, zfar );
    camera.position.set( - 1.8, 0.6, 2.7 );

    scene = new THREE.Scene();

    raycaster = new THREE.Raycaster();
    clock = new THREE.Clock();

    //settings names 

    new THREE.TextureLoader()
        .setPath( "/assets/textures/" )
        .load( 'digital_painting_underwater_light.jpg', function ( texture ) {

            texture.mapping = THREE.EquirectangularReflectionMapping;

            scene.background = texture;
            scene.environment = texture;
            render();
            const loader = new GLTFLoader().setPath( '/assets/models/' );

            loader.load('trampo.glb', function ( gltf ) {
                trampo = gltf.scene;
                trampo.children.forEach(child => {
                    planeBoxes.push(new THREE.Box3().setFromObject(child));
                    child.material.side= THREE.DoubleSide
                });
                scene.add( trampo );
            } );

            loader.load( 'pieuvre_clement_athimon_bones_8.glb', function ( gltf ) {
                octopus = gltf.scene;
                scene.add( octopus );

                bonesNames.forEach(name => {
                    bones.set(name,[]);
                    let bone = scene.getObjectByName(name);
                    bones.get(name)?.push(bone);
                    bone = bone.children;
                    while(!(bone === undefined || bone.length == 0)){
                        bones.get(name)?.push(bone[0]);
                        bone = bone[0].children;
                    }   
                });
                octoBox.setFromObject(octopus,true);
                animate();
            } );

    
           
        } );
  

        function addCube(px, py, pz) {
            var colorandom = new THREE.Color(0xffffff);
            colorandom.setHex(Math.random() * 0xffffff);
            var geometry = new THREE.BoxGeometry(px, py, pz); //x,y,z
            var boxMaterial = new THREE.MeshBasicMaterial({ color: colorandom });
            var cube = new THREE.Mesh(geometry, boxMaterial);

            cube.position.set(0,0,0);
            cube.geometry.computeBoundingBox(); // null sinon
            console.log(cube)
            scene.add(cube);
            return cube;
        }

    renderer = new THREE.WebGLRenderer( { antialias: true } );
    renderer.setPixelRatio( window.devicePixelRatio );
    renderer.setSize( window.innerWidth, window.innerHeight );
    renderer.toneMapping = THREE.ACESFilmicToneMapping;
    renderer.toneMappingExposure = 1;
    renderer.outputEncoding = THREE.sRGBEncoding;
    container.appendChild( renderer.domElement );
    
    const controls = new OrbitControls( camera, renderer.domElement );
    controls.addEventListener( 'change', render ); // use if there is no animation loop
    controls.minDistance = 2;
    controls.maxDistance = 200;
    controls.target.set( 0, 0, - 0.2 );
    controls.update();

    window.addEventListener( 'resize', onWindowResize );
    //window.addEventListener('click' ,setClickPosition);
}


function onWindowResize() {

    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();

    renderer.setSize( window.innerWidth, window.innerHeight );

    animate();

}

function render() {

    renderer.render( scene, camera );

}

function animate(){

    if(trampo != undefined){
        //box.copy( trampo.children[0].geometry.boundingBox ).applyMatrix4( trampo.children[0].matrixWorld );
    }
    if(octopus != undefined){
        octopus.position.y+=yMovement;
        octopus.position.z+=zMovement;
        octoBox.setFromObject(octopus);
        let rank = 0;
        planeBoxes.forEach(planeBox => {
            if(octoBox.intersectsBox(planeBox)){
                const geometryArray = octopus.position;
				let interactedObject = trampo.children[rank];
                let normal = interactedObject.normalMatrix.elements
                if( Math.floor(Math.random() * 1) > 0.8){
                    zMovement = normal[1]*1.2;
                }else{
                    zMovement = normal[1];
                }
                yMovement = normal[7];
				/*geometryArray[3*faces.a] += movement * normal.x;
				geometryArray[3*faces.a+1] += movement * normal.y;
				geometryArray[3*faces.a+2] += movement * normal.z;
				geometryArray[3*faces.b] += movement * normal.x;
				geometryArray[3*faces.b+1] +=  movement * normal.y;
				geometryArray[3*faces.b+2] += movement * normal.z;
				geometryArray[3*faces.c] += movement * normal.x;
				geometryArray[3*faces.c+1] += movement * normal.y;
				geometryArray[3*faces.c+2] += movement * normal.z;
                */
				octopus.position.needsUpdate = true;
                return;
            }
            rank++;
        });

    }
    
    requestAnimationFrame(animate);
    
    const quaternion = new THREE.Quaternion();
    if(frameCounter > 120){
        quaterRotation = -quaterRotation*.0001
        frameCounter = 0
    }
    frameCounter++
    quaternion.setFromAxisAngle(new THREE.Vector3(2,10,3),quaterRotation)
    bones.forEach((arm) =>{
        arm.forEach(bone =>{
            bone?.applyQuaternion(quaternion);
        });
    })
    /*
    betaAnim += 0.01;

    let changeposition = 0;
    //let order = 30/7;

    bones.forEach((arm) => {
        let changeposition = 0;
        arm.forEach(bone =>{
            changeposition += 2*(Math.PI/35)
            bone.position.x = 0.1*Math.sin((changeposition)+betaAnim);

        });
        
        //bone.rotation.z = 0.1*Math.sin((changeposition)+betaAnim);
        //bone.rotation.y = 0.1*Math.sin((changeposition)+betaAnim);
    });*/

    /*bones.forEach(bone => {
        changeposition = changeposition+order 
        if(Math.abs(changeposition) == 30){
            order = -order;
        }
        //bone.rotation.y = Math.abs( Math.cos( Date.now() * 0.00005))
        bone.position.x += Math.cos(changeposition *(Math.PI/3))*0.01;
        bone.position.x += Math.cos(changeposition *(Math.PI/3))*0.01;
        console.log(bone.position.x)
        //bone.position.x += Math.cos(Date.now()*0.0005*order)*0.01;
    });*/

    
    renderer.render(scene, camera);
}